import React, { useState, useEffect } from "react";
import "./App.css";
import axios from "axios";
import breadRecipes from "./breadRecipes";
import BreadSelect from "./Control/BreadSelect";
import Flour from "./Control/Flour";
import Breadingredients from "./Control/BreadIngredients";
import BreadPreperation from "./Control/BreadPreperation";
import { AxiosResponse } from "axios";
import { BreadRecipeType } from "./interfaces/breadrecipe";

const App = () => {
  useEffect(() => {
    getLogo();
  }, []);
  useEffect(() => {
    setCurrentRecipe();
  });

  const [flour, setFlour] = useState(500);
  const [breadLogo, setBreadLogo] = useState(null);
  const [recipe, setRecipe] = useState({} as BreadRecipeType);
  const [selectedBreadTitle, setSelectedBreadTitle] = useState('');
  const headerStyle = {
    backgroundImage: `url(${breadLogo})`,
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
  };

  const getLogo = () => {
    axios
      .get("https://api.unsplash.com/photos/random", {
        params: {
          client_id: "Tu4EqFZ28zGqh07es4ssr7Lpczsn7sEpcmgxJqESliQ",
          query: "bread",
        },
      })
      .then((response: AxiosResponse) => {
        setBreadLogo(response.data.urls.regular);
        console.log(response.data.urls);
      });
  };

  const onChange = (e: React.FormEvent<HTMLSelectElement>) => {
    const newTitle: string = e.currentTarget.value;
    setSelectedBreadTitle(newTitle);
  };

  const setCurrentRecipe = () => {
    const currentRecipe: BreadRecipeType | undefined = breadRecipes.find(
      (item) => item.title === selectedBreadTitle
    );
    currentRecipe ? setRecipe(currentRecipe) : null;
  };

  return (
    <div className="App">
      <header className="App-header" style={headerStyle}>
        <h1>For Bread Baking</h1>
        {/* <img
          src={breadLogo}
          className="img-fluid rounded float-start"
          alt="logo"
        /> */}
      </header>
      <div className="container recipe">
        <div className="row">
          <div className="col">
            <form>
              <div className="form-group">
                <label className="form-label">Choose a Bread</label>
                <select
                  className="form-control"
                  id="formSelect"
                  onChange={onChange}
                >
                  <option>Select a Bread</option>
                  <BreadSelect />
                </select>
              </div>
              <Flour flour={flour} changeFlour={(e) => setFlour(parseInt(e.currentTarget.value))} />
            </form>
          </div>
          <div className="col">
            <Breadingredients recipe={recipe} flour={flour} />
          </div>
        </div>
      </div>
      <div className="container preperation">
        <div className="row">
          <div className="col">
            <BreadPreperation recipe={recipe} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default App;
