import React from "react";

const Flour = (props: { flour: number; changeFlour: React.ChangeEventHandler<HTMLInputElement> }) => {
  return (
    <div className="form-group">
      <label htmlFor="flour">Flour</label>
      <input
        type="number"
        className="form-control"
        id="flour"
        name="flour"
        step="50"
        min="100"
        placeholder="500"
        value={props.flour}
        onChange={props.changeFlour}
      />
    </div>
  );
};
export default Flour;
