import React from "react";
import { BreadRecipeType } from "../interfaces/breadrecipe";

const Yeast = (
  props: 
  { 
    recipe: BreadRecipeType; flour: number }) => {
  return (
    <li className="list-group-item">
      <b>Yeast</b>{" "}
      {Math.round(
        ((props.recipe.ingredients.yeast! / 100) * props.flour * 100) / 100
      )}
    </li>
  );
};

export default Yeast;
