import React from "react";
import { BreadRecipeType } from "../interfaces/breadrecipe";

const Leaven = (props: { recipe: BreadRecipeType; flour: number; }) => {
  return (
    <li className="list-group-item">
      <b>Leaven</b>{" "}
      {Math.round(
        ((props.recipe.ingredients.leaven! / 100) * props.flour * 100) / 100
      )}
    </li>
  );
};

export default Leaven;
