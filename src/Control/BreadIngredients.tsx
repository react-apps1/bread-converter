import React from "react";
import Yeast from "./Yeast";
import Leaven from "./Leaven";
import { BreadRecipeType } from "../interfaces/breadrecipe";

const Breadingredients = (props: { recipe: BreadRecipeType, flour: number }) => {
  const { recipe, flour } = props
  if (recipe === undefined)
    return (
      <div className="container row justify-content-md-center lead">
        All numbers are based on 100% assumption
      </div>
    );
  if (!recipe.ingredients)
    return (
      <div className="row justify-content-md-center container lead">
        All numbers are based on 100% assumption
      </div>
    );
  if (Number.isNaN(recipe.ingredients.yeast)) {
    return;
  }

  return (
    <div className="list-group">
      <h3>{recipe.title}</h3>
      <ul className="list-group list-group-flush">
        <li className="list-group-item">
          <b>White Flour:</b>{" "}
          {Math.round(
            ((recipe.ingredients.whiteFlour / 100) * flour * 100) / 100
          )}
        </li>
        <li className="list-group-item">
          <b>Whole Wheat Flour</b>{" "}
          {Math.round(
            ((recipe.ingredients.wholeWheatFlour / 100) * flour * 100) / 100
          )}
        </li>
        <li className="list-group-item">
          <b>Water</b>{" "}
          {Math.round(((recipe.ingredients.water / 100) * flour * 100) / 100)}
        </li>
        {recipe.ingredients.leaven ? (
          <Leaven recipe={recipe} flour={flour} />
        ) : (
          <Yeast recipe={recipe} flour={flour} />
        )}
        <li className="list-group-item">
          <b>Salt</b>{" "}
          {Math.round(((recipe.ingredients.salt / 100) * flour * 100) / 100)}
        </li>
      </ul>
    </div>
  );
};
export default Breadingredients;
