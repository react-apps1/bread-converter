import React from 'react';
import breadRecipes from "../breadRecipes";

//Renders bread select dropdown

const BreadSelect = () => {
  return breadRecipes.map((item) => (
    <option key={item.title} value={item.title}>
      {item.title}
    </option>
  ));
};

export default BreadSelect;
