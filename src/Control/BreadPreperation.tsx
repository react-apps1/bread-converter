import React from "react";
import { BreadRecipeType } from "../interfaces/breadrecipe";

const BreadPreperation = (props: { recipe: BreadRecipeType; }) => {
  const recipe = props.recipe;
  if (recipe === undefined)
    return (
      <div className="row justify-content-md-center container display-4">
        Preperation instructions will appear when you choose a bread!
      </div>
    );
  const preperation = recipe.preperation;
  if (!preperation)
    return (
      <div className="row justify-content-md-center container display-4">
        Preperation instructions will appear when you choose a bread!
      </div>
    );
  console.log(preperation);
  if (Number.isNaN(recipe.ingredients.yeast)) {
    return;
  }

  return (
    <div className="list-group">
      <table className="table table-striped">
        <thead className="thead-dark">
          <tr>
            <th scope="col">Type</th>
            <th scope="col">Time</th>
            <th scope="col">Temperature</th>
          </tr>
        </thead>
        <tbody>
          {preperation.autolyse ? (
            <tr>
              <th scope="row">Autolyse</th>
              <td>{preperation.autolyse.time} minutes</td>
              <td>{preperation.autolyse?.temperature} F</td>
            </tr>
          ) : undefined}
          {preperation.bulkFermentation ? (
            <tr>
              <th scope="row">Bulk Fermentation</th>
              <td>{preperation.bulkFermentation.time} minutes</td>
              <td>{preperation.bulkFermentation.temperature} F</td>
            </tr>
          ) : undefined}
          {preperation.proof ? (
            <tr>
              <th scope="row">Proof</th>
              <td>{preperation.proof.time} hours</td>
              <td>{preperation.proof.temperature} F</td>
            </tr>
          ) : undefined}
          {preperation.bake ? (
            <tr>
              <th scope="row">Bake</th>
              <td>{preperation.bake.time} minutes</td>
              <td>{preperation.bake.temperature} F</td>
            </tr>
          ) : undefined}
          <tr>
            <th scope="row">Total</th>
            <td>{preperation.total} hours</td>
            <td>---</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};
export default BreadPreperation;
