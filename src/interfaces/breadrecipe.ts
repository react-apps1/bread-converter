export interface BreadRecipeType 
{
    author: string;
    title: string;
    type: string;
    ingredients: {
        whiteFlour: number;
        wholeWheatFlour: number;
        leaven?: number;
        water: number;
        milk?: number;
        yeast?: number;
        salt: number;
        eggs?: number;
        butter?: number;
        sugar?: number;
        tangzhong?: {
            "bread-flour": number,
            water: number,
            milk: number,
          },
    },
    preperation: {
        autolyse?: {
            time: number,
            temperature: number,
        },
        bulkFermentation: {
            time: number,
            temperature: number,
        },
        proof?: {
            time: number,
            temperature: number,
        },
        bake: {
            time: number,
            temperature: number,
        },
        total: number,
    }
}